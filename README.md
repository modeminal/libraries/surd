# Surd

Simple and minimal command line argument parser for Rust.

## Links

- [Online Documentation](https://docs.rs/surd/)
- [Crates.io](https://crates.io/crates/surd)

## Example Code

```rust
extern crate surd;

fn main() {
    // Create a new surd app.
    let mut app = surd::Surd::new("test", "a test app", "aiocat", "1.0.0");
    app.add_flag("t", "a test flag", Vec::new());
    app.set_handler(run_main);

    // Create a sub-command for main app.
    let mut test_command = surd::SubCommand::new("test", "a test command");
    test_command.add_usage("test test");
    test_command.add_flag("at", "another test flag", vec!["atest".to_string()]);
    test_command.add_alias("t".to_string());
    test_command.set_handler(run_test);

    app.add_command(test_command);

    // Handle the command.
    app.start();
}

fn run_main(found_flags: &Vec<surd::Flag>, found_args: Vec<String>) {
    println!("main: {:?}", found_args);

    for i in found_flags.iter() {
        println!("{} - {}", i.description, i.value);
    }
}

fn run_test(found_flags: &Vec<surd::Flag>, found_args: Vec<String>) {
    println!("test: {:?}", found_args);

    for i in found_flags.iter() {
        println!("{} - {}", i.description, i.value);
    }
}
```

## Found a Bug / Error?

If you found a bug or an error, please create a new issue at gitlab repository.

## Contributing

If you want to contribute this project:

- Make sure you add the comments for your codes.
- Please do not something useless.

## Authors

- [Modeminal](https://gitlab.com/modeminal)

## License

This project is distributed under MIT license.

## Project status

Under development.
