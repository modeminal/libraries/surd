#[derive(Debug)]
// Flag struct for app and sub-commands.
pub struct Flag {
    pub name: &'static str,
    pub description: &'static str,
    pub aliases: Vec<String>,
    pub value: String,
}

// Sub-command struct.
pub struct SubCommand {
    pub name: &'static str,
    pub description: &'static str,

    pub usages: Vec<&'static str>,
    pub aliases: Vec<String>,
    pub flags: Vec<Flag>,

    pub handle: fn(found_flags: &Vec<Flag>, found_args: Vec<String>),
}

impl SubCommand {
    /// Create a new sub-command.
    ///
    /// # Arguments
    ///
    /// * `name` (`&'static str`) - Command name.
    /// * `description` (`&'static str`) - Command description.
    ///
    /// # Examples
    ///
    /// ```
    /// let mut test_command = surd::SubCommand::new("test", "a test command");
    /// ```
    pub fn new(name: &'static str, description: &'static str) -> SubCommand {
        SubCommand{
            name: name,
            description: description,
            usages: Vec::new(),
            aliases: Vec::new(),
            flags: Vec::new(),
            handle: |_ff: &Vec<Flag>, _fa: Vec<String>| {},
        }
    }

    /// Add an usage for sub-command.
    ///
    /// # Arguments
    ///
    /// * `usage` (`&'static str`) - Command usage.
    ///
    /// # Examples
    ///
    /// ```
    /// test_command.add_usage("app test [-m=value]");
    /// ```
    pub fn add_usage(&mut self, usage: &'static str) {
        self.usages.push(usage)
    }

    /// Add an alias for sub-command.
    ///
    /// # Arguments
    ///
    /// * `alias` (`String`) - New alias.
    ///
    /// # Examples
    ///
    /// ```
    /// test_command.add_alias(String::from("t"));
    /// ```
    pub fn add_alias(&mut self, alias: String) {
        self.aliases.push(alias)
    }

    /// Add a new flag for sub-command.
    ///
    /// # Arguments
    ///
    /// * `name` (`&'static str`) - The flag name.
    /// * `description` (`&'static str`) - The flag description.
    /// * `aliases` (`Vec<String>`) - Flag aliases (other names).
    ///
    /// # Examples
    ///
    /// ```
    /// test_command.add_flag("minimize", "minimize the output", vec![String::from("m")]);
    /// ```
    pub fn add_flag(&mut self, name: &'static str, description: &'static str, aliases: Vec<String>) {
        self.flags.push(Flag{
            name: name,
            description: description,
            aliases: aliases,
            value: "".to_string()
        })
    }

    /// Set the execute function for the sub-command.
    ///
    /// # Arguments
    ///
    /// * `handler` (`fn(found_flags: &Vec<Flag>, found_args: Vec<String>)`) - The function will be executed.
    ///
    /// # Examples
    ///
    /// ```
    /// test_command.set_handler(run_test_c);
    /// 
    /// // ...
    /// 
    /// fn run_test_c(found_flags: &Vec<surd::Flag>, found_args: Vec<String>) {
    ///     println!("test: {:?}", found_args);
    ///
    ///     for i in found_flags.iter() {
    ///         println!("{} - {}", i.description, i.value);
    ///     }
    /// }
    /// ```
    pub fn set_handler(
        &mut self,
        handler: fn(found_flags: &Vec<Flag>, found_args: Vec<String>),
    ) {
        self.handle = handler
    }
}