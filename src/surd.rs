use crate::types::{SubCommand, Flag};

/// The main app struct.
pub struct Surd {
    pub name: &'static str,
    pub description: &'static str,
    pub author: &'static str,
    pub version: &'static str,

    pub usages: Vec<&'static str>,
    pub flags: Vec<Flag>,
    pub sub_commands: Vec<SubCommand>,

    pub handle: fn(found_flags: &Vec<Flag>, found_args: Vec<String>),
}

impl Surd {
    /// Create a new main app.
    ///
    /// # Arguments
    ///
    /// * `name` (`&'static str`) - App name.
    /// * `desc` (`&'static str`) - App description.
    /// * `author` (`&'static str`) - App author.
    /// * `version` (`&'static str`) - App version.
    ///
    /// # Examples
    ///
    /// ```
    /// let mut app = surd::Surd::new("test", "a test app", "aiocat", "1.0.0");
    /// ```
    pub fn new(
        name: &'static str,
        desc: &'static str,
        author: &'static str,
        version: &'static str,
    ) -> Surd {
        Surd {
            name: name,
            description: desc,
            author: author,
            version: version,
            usages: Vec::new(),
            flags: Vec::new(),
            sub_commands: Vec::new(),
            handle: |_ff: &Vec<Flag>, _fa: Vec<String>| {},
        }
    }

    /// Set the execute function for main app.
    ///
    /// # Arguments
    ///
    /// * `handler` (`fn(found_flags: &Vec<Flag>, found_args: Vec<String>)`) - The function will be executed.
    ///
    /// # Examples
    ///
    /// ```
    /// app.set_handler(run_main);
    /// 
    /// // ...
    /// 
    /// fn run_main(found_flags: &Vec<surd::Flag>, found_args: Vec<String>) {
    ///     println!("main: {:?}", found_args);
    ///
    ///     for i in found_flags.iter() {
    ///         println!("{} - {}", i.description, i.value);
    ///     }
    /// }
    /// ```
    pub fn set_handler(
        &mut self,
        handler: fn(found_flags: &Vec<Flag>, found_args: Vec<String>),
    ) {
        self.handle = handler
    }

    /// Add a new flag for the main app.
    ///
    /// # Arguments
    ///
    /// * `name` (`&'static str`) - The flag name.
    /// * `description` (`&'static str`) - The flag description.
    /// * `aliases` (`Vec<String>`) - Flag aliases (other names).
    ///
    /// # Examples
    ///
    /// ```
    /// app.add_flag("minimize", "minimize the output", vec![String::from("m")]);
    /// ```
    pub fn add_flag(&mut self, name: &'static str, description: &'static str, aliases: Vec<String>) {
        self.flags.push(Flag{
            name: name,
            description: description,
            aliases: aliases,
            value: "".to_string()
        })
    }

    /// Add a new sub-command to the main app.
    ///
    /// # Arguments
    ///
    /// * `new_command` (`SubCommand`) - Sub-command struct.
    ///
    /// # Examples
    ///
    /// ```
    /// let mut test_command = surd::SubCommand::new("test", "a test command");
    /// 
    /// // ...
    /// 
    /// app.add_flag("minimize", "minimize the output", vec![String::from("m")]);
    /// ```
    pub fn add_command(&mut self, new_command: SubCommand) {
        self.sub_commands.push(new_command)
    }

    /// Start the command line parser.
    ///
    /// # Examples
    ///
    /// ```
    /// app.start();
    /// ```
    pub fn start(&self) {
        let mut program_arguments: Vec<String> = std::env::args().collect();
        program_arguments.remove(0);

        let mut found_arguments: Vec<String> = Vec::new();
        let mut found_raw_flags: Vec<String> = Vec::new();

        for argument in program_arguments {
            if !argument.starts_with("-") {
                found_arguments.push(argument);
            } else {
                found_raw_flags.push(argument);
            }
        }

        let mut parsed_flags: Vec<Flag> = Vec::new();
        let mut is_found = false;

        if found_arguments.len() > 0 {
            let command_name = &found_arguments.clone()[0];

            for sub_command in &self.sub_commands {
                if sub_command.name == command_name || self.check_has_alias(&sub_command.aliases, command_name) {
                    is_found = true;
                    
                    self.handle_flags_for_command(&found_raw_flags, &mut parsed_flags, sub_command);
                    (sub_command.handle)(&parsed_flags, found_arguments.clone());
                    break;
                }
            }
        }
        
        if is_found == false || found_arguments.len() == 0 {
            self.handle_flags(&found_raw_flags, &mut parsed_flags);
            (self.handle)(&parsed_flags, found_arguments.clone())
        }
    }

    fn handle_flags_for_command(&self, found_raw_flags: &Vec<String>, write_to: &mut Vec<Flag>, sub_command: &SubCommand) {
        for flag in found_raw_flags {
            match flag.split_once("=") {
                Some(i) => {
                    let (mut flag_name, found_value) = i;

                    let flag_str = flag_name.to_string();
                    flag_name = flag_str.trim_start_matches("-");

                    let found_flags: Vec<Flag> = sub_command.flags.iter()
                    .filter(|pf| pf.name == flag_name || self.check_has_alias(&pf.aliases, flag_name))
                    .map(|pf| Flag{
                          name: pf.name,
                          description: pf.description,
                          aliases: pf.aliases.clone(),
                          value: found_value.to_string()
                      }).collect();

                    (*write_to).extend(found_flags);
                },
                None => {
                    let flag_name = flag.trim_start_matches("-");

                    let found_flags: Vec<Flag> = sub_command.flags.iter()
                    .filter(|pf| pf.name == flag_name || self.check_has_alias(&pf.aliases, flag_name))
                    .map(|pf| Flag{
                          name: pf.name,
                          description: pf.description,
                          aliases: pf.aliases.clone(),
                          value: "".to_string()
                      }).collect();

                    (*write_to).extend(found_flags);
                }
            }
        }
    }

    fn handle_flags(&self, found_raw_flags: &Vec<String>, write_to: &mut Vec<Flag>) {
        for flag in found_raw_flags {
            match flag.split_once("=") {
                Some(i) => {
                    let (mut flag_name, found_value) = i;

                    let flag_str = flag_name.to_string();
                    flag_name = flag_str.trim_start_matches("-");

                    let found_flags: Vec<Flag> = self.flags.iter()
                    .filter(|pf| pf.name == flag_name || self.check_has_alias(&pf.aliases, flag_name))
                    .map(|pf| Flag{
                          name: pf.name,
                          description: pf.description,
                          aliases: pf.aliases.clone(),
                          value: found_value.to_string()
                      }).collect();

                    (*write_to).extend(found_flags);
                },
                None => {
                    let flag_name = flag.trim_start_matches("-");

                    let found_flags: Vec<Flag> = self.flags.iter()
                    .filter(|pf| pf.name == flag_name || self.check_has_alias(&pf.aliases, flag_name))
                    .map(|pf| Flag{
                          name: pf.name,
                          description: pf.description,
                          aliases: pf.aliases.clone(),
                          value: "".to_string()
                      }).collect();

                    (*write_to).extend(found_flags);
                }
            }
        }
    }

    fn check_has_alias(&self, aliases: &Vec<String>, will_controlled: &str) -> bool {
        for alias in aliases {
            if alias == will_controlled {
                return true
            }
        }

        return false
    }
}
